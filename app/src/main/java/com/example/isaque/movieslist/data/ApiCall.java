package com.example.isaque.movieslist.data;

import com.example.isaque.movieslist.model.MovieDetailsData;
import com.example.isaque.movieslist.model.MoviesListData;

import java.io.IOException;
import java.util.Locale;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

import static com.example.isaque.movieslist.data.ServiceAPI.API_KEY;


public class ApiCall {

    private ServiceAPI serviceAPI;
    private String language;

    public ApiCall() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ServiceAPI.BASE_URL).
                addConverterFactory(GsonConverterFactory.create()).build();
        this.serviceAPI = retrofit.create(ServiceAPI.class);
        this.language = Locale.getDefault().getLanguage();
    }

    public MovieDetailsData getMovieByID(int id) throws IOException {
        return serviceAPI.getMovieByID(id, API_KEY, language).execute().body();
    }

    public MoviesListData getPopularMovies(int page) throws IOException {
        return serviceAPI.getPopularMovies(API_KEY, page, language).execute().body();
    }

    public MoviesListData getMoviesListBySearch(String query, int page) throws IOException {
        return serviceAPI.getMoviesListBySearch(API_KEY, query, page, language).
                execute().body();
    }

    public MoviesListData getUpcomingMovies(int page) throws IOException {
        return serviceAPI.getUpcomingMovies(API_KEY, page, language).execute().body();
    }

    public MoviesListData getPlayingNowMovies(int page) throws IOException {
        return serviceAPI.getNowPlayingMovies(API_KEY, page, language).execute().body();
    }

}
