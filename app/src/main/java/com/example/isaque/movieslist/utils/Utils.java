package com.example.isaque.movieslist.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.isaque.movieslist.data.ServiceAPI;
import com.example.isaque.movieslist.model.MovieDetailsData;

import java.util.List;

public class Utils {

    public static String getPosterLink(String imagePath, String size){
        return ServiceAPI.IMAGES_BASE_URL + size + imagePath;
    }

    public static String getIMDbLink(String imdbLink){
        return "http://www.imdb.com/title/" + imdbLink;
    }

    public static String getFormattedGenres(List<MovieDetailsData.GenreData> genres){
        if (genres == null) return "";

        StringBuilder formattedString = new StringBuilder();

        for (int i = 0; i < genres.size(); i++){
            formattedString.append(genres.get(i).getName());
            if (i != genres.size()-1) {
                formattedString.append(", ");
            }
        }
        return formattedString.toString();
    }

    public static boolean isConnected(Context context){
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

}
