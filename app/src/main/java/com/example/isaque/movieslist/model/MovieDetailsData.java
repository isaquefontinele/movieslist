package com.example.isaque.movieslist.model;

import java.io.Serializable;
import java.util.List;

public class MovieDetailsData implements Serializable {

    private int budget;
    private int id;
    private int revenue;
    private String homepage;
    private String overview;
    private String poster_path;
    private String backdrop_path;
    private String release_date;
    private String title;
    private String original_title;
    private String imdb_id;
    private double popularity;
    private double vote_average;
    private List<GenreData> genres;

    public int getBudget() {
        return budget;
    }

    public List<GenreData> getGenres() {
        return genres;
    }

    public String getHomepage() {
        return homepage;
    }

    public int getId() {
        return id;
    }

    public String getOverview() {
        return overview;
    }

    public double getPopularity() {
        return popularity;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public String getRelease_date() {
        return release_date;
    }

    public int getRevenue() {
        return revenue;
    }

    public String getTitle() {
        return title;
    }

    public double getVote_average() {
        return vote_average;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public String getImdb_id() {
        return imdb_id;
    }

    public String getOriginal_title() {
        return original_title;
    }

    //Inner class
    public static class GenreData {

        private int id;
        private String name;

        public GenreData(){}

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
