package com.example.isaque.movieslist.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.example.isaque.movieslist.R;
import com.example.isaque.movieslist.data.MoviesRetrievementService;
import com.example.isaque.movieslist.model.MovieDetailsData;
import com.example.isaque.movieslist.utils.Constants;
import com.example.isaque.movieslist.view.MovieDetailFragment;

import static com.example.isaque.movieslist.utils.Constants.ERROR;

public class MovieDetailActivity extends BaseActivity {


    private MovieDetailActivity.MovieResultReceiverCallback mReceiver;
    private MovieDetailsData mMovie;
    private Bundle instanceState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.instanceState = savedInstanceState;
        setContentView(R.layout.activity_movie_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        setupReceiver();
    }

    private void setupReceiver(){
        mMovie = new MovieDetailsData();
        mReceiver = new MovieDetailActivity.MovieResultReceiverCallback(new Handler());
        Intent intent = new Intent(this, MoviesRetrievementService.class);
        intent.setAction(Constants.ACTION_GET_MOVIE);
        intent.putExtra(Constants.MOVIE_ID, getIntent().getIntExtra(Constants.MOVIE_ID, -1));
        intent.putExtra(Constants.RESULT_RECEIVER, mReceiver);
        startService(intent);
        showLoading();
    }

    private void setupFragment(Bundle savedInstanceState){
        if (savedInstanceState == null) {
            Bundle arguments = new Bundle();
            arguments.putSerializable(Constants.MOVIE_DATA, mMovie);
            MovieDetailFragment fragment = new MovieDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.movie_detail_container, fragment)
                    .commit();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            navigateUpTo(new Intent(this, MovieListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        ImageView fullscreenPoster = (ImageView) findViewById(R.id.movie_poster_fullscreen);

        if (fullscreenPoster.getVisibility() == View.VISIBLE) {
            fullscreenPoster.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }

    private class MovieResultReceiverCallback<T> extends ResultReceiver {
        MovieResultReceiverCallback(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            hideLoading();
            if (mReceiver != null){
                if (resultCode == Constants.RESULT_OK) {
                    mMovie = ((MovieDetailsData)
                            resultData.getSerializable(Constants.ACTION_GET_MOVIE));

                } else {
                    showError(resultData.getString(ERROR));
                    mMovie = null;
                }
                setupFragment(instanceState);
            }
        }
    }
}
