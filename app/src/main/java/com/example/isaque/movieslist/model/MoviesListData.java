package com.example.isaque.movieslist.model;

import java.io.Serializable;
import java.util.List;

public class MoviesListData implements Serializable {

    private List<MovieData> results;

    public List<MovieData> getMoviesDataList() {
        return results;
    }

}
