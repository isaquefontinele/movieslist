package com.example.isaque.movieslist.data;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;

import com.example.isaque.movieslist.model.MoviesListData;
import com.example.isaque.movieslist.utils.Constants;

import static com.example.isaque.movieslist.utils.Constants.ACTION_FLAG;
import static com.example.isaque.movieslist.utils.Constants.ACTION_GET_MOVIE;
import static com.example.isaque.movieslist.utils.Constants.ACTION_GET_NOW_PLAYING;
import static com.example.isaque.movieslist.utils.Constants.ACTION_GET_POPULAR;
import static com.example.isaque.movieslist.utils.Constants.ACTION_GET_SEARCH;
import static com.example.isaque.movieslist.utils.Constants.ACTION_GET_UPCOMING;
import static com.example.isaque.movieslist.utils.Constants.ERROR;
import static com.example.isaque.movieslist.utils.Constants.ERROR_GETTING_DATA;
import static com.example.isaque.movieslist.utils.Constants.NETWORK_CONNECTION_ERROR;
import static com.example.isaque.movieslist.utils.Constants.RESULT_FAIL;
import static com.example.isaque.movieslist.utils.Constants.RESULT_OK;
import static com.example.isaque.movieslist.utils.Utils.*;

public class MoviesRetrievementService extends IntentService {

    public MoviesRetrievementService() {
        super("MoviesRetrievementService");
    }

    /** Instância do call back */
    ResultReceiver resultReceiver;
    /** Flag indicando qual serviço será chamado */
    String action;
    /** Id do filme */
    int movieId;
    /** Page of the list */
    int page;
    /** Texto da search box. */
    String searchQuery;

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        resultReceiver = (ResultReceiver) intent.getExtras().get(Constants.RESULT_RECEIVER);
        action = intent.getAction();
        movieId = intent.getIntExtra(Constants.MOVIE_ID, -1);
        page = intent.getIntExtra(Constants.PAGE, 1);
        searchQuery = intent.getStringExtra(Constants.SEARCH_QUERY);


        final ApiCall api = new ApiCall();
        final Bundle bundle = new Bundle();
        MoviesListData moviesListData;

        if (!isConnected(getApplicationContext())) {
            bundle.putSerializable(ERROR, NETWORK_CONNECTION_ERROR);
            resultReceiver.send(RESULT_FAIL, bundle);
            return;
        }

        try{
            switch (action){
                case ACTION_GET_POPULAR:
                    moviesListData = api.getPopularMovies(page);
                    saveMoviesListDataOnBundle(bundle, moviesListData, ACTION_GET_POPULAR);
                    break;
                case ACTION_GET_MOVIE:
                    bundle.putSerializable(ACTION_GET_MOVIE, api.getMovieByID(movieId));
                    bundle.putString(ACTION_FLAG, ACTION_GET_MOVIE);
                    resultReceiver.send(RESULT_OK, bundle);
                    break;
                case ACTION_GET_SEARCH:
                    bundle.putSerializable(ACTION_GET_SEARCH, api.getMoviesListBySearch(searchQuery, page));
                    bundle.putString(ACTION_FLAG, ACTION_GET_SEARCH);
                    resultReceiver.send(RESULT_OK, bundle);
                    break;
                case ACTION_GET_UPCOMING:
                    moviesListData = api.getUpcomingMovies(page);
                    saveMoviesListDataOnBundle(bundle, moviesListData, ACTION_GET_UPCOMING);
                    break;
                case ACTION_GET_NOW_PLAYING:
                    moviesListData = api.getPlayingNowMovies(page);
                    saveMoviesListDataOnBundle(bundle, moviesListData, ACTION_GET_NOW_PLAYING);
                    break;
                default:
                    bundle.putString(ERROR, ERROR_GETTING_DATA);
                    resultReceiver.send(RESULT_FAIL, bundle);
                    break;
            }

        } catch (Exception e){
            e.printStackTrace();
            bundle.putString(ERROR, ERROR_GETTING_DATA);
            resultReceiver.send(RESULT_FAIL, bundle);
        }
    }

    private void saveMoviesListDataOnBundle(Bundle bundle, MoviesListData moviesListData, String actionFlag){
        if (moviesListData != null) {
            bundle.putSerializable(actionFlag, moviesListData);
            bundle.putString(ACTION_FLAG, actionFlag);
            resultReceiver.send(RESULT_OK, bundle);
        } else {
            bundle.putString(ERROR, ERROR_GETTING_DATA);
            resultReceiver.send(RESULT_FAIL, bundle);
        }
    }
}
