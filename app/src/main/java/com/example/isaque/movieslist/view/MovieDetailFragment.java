package com.example.isaque.movieslist.view;

import android.app.Activity;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.isaque.movieslist.activities.MovieDetailActivity;
import com.example.isaque.movieslist.activities.MovieListActivity;
import com.example.isaque.movieslist.R;
import com.example.isaque.movieslist.data.ServiceAPI;
import com.example.isaque.movieslist.model.MovieDetailsData;
import com.example.isaque.movieslist.utils.Constants;
import com.example.isaque.movieslist.utils.Utils;
import com.squareup.picasso.Picasso;

/**
 * A fragment representing a single Movie detail screen.
 * This fragment is either contained in a {@link MovieListActivity}
 * in two-pane mode (on tablets) or a {@link MovieDetailActivity}
 * on handsets.
 */
public class MovieDetailFragment extends Fragment {

    private MovieDetailsData mMovie;
    ImageView moviePosterFullscreen;
    ImageView moviePosterMini;

    public MovieDetailFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(Constants.MOVIE_DATA)) {
            mMovie = (MovieDetailsData) getArguments().getSerializable(Constants.MOVIE_DATA);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.movie_detail, container, false);
        setUpToolbar();

        final LinearLayout movieDetails = (LinearLayout) rootView.findViewById(R.id.details_body);
        final TextView movieNotFound = (TextView) rootView.findViewById(R.id.movie_details_not_found);
        final TextView movieName = (TextView) rootView.findViewById(R.id.name);
        final TextView releaseDate = (TextView) rootView.findViewById(R.id.release_date);
        final TextView rating = (TextView) rootView.findViewById(R.id.rating);
        final TextView generos = (TextView) rootView.findViewById(R.id.generos);
        final TextView sinopse = (TextView) rootView.findViewById(R.id.movie_sinopse);
        final TextView movieID = (TextView) rootView.findViewById(R.id.movieID);

        if (mMovie != null) {
            movieName.setText(mMovie.getOriginal_title());
            movieID.setText(String.valueOf(mMovie.getId()));
            releaseDate.setText(mMovie.getRelease_date());
            generos.setText(Utils.getFormattedGenres(mMovie.getGenres()));
            rating.setText(String.valueOf(mMovie.getVote_average()));

            if (mMovie.getOverview() != null && !mMovie.getOverview().isEmpty()){
                sinopse.setText(mMovie.getOverview());
            } else {
                sinopse.setText(R.string.overview_not_found);
            }

            setUpPoster(rootView);
            setUpLinks(rootView);
        } else {
            movieNotFound.setVisibility(View.VISIBLE);
            movieDetails.setVisibility(View.GONE);
        }
        return rootView;
    }

    private void setUpToolbar(){
        Activity activity = this.getActivity();
        CollapsingToolbarLayout appBarLayout =
                (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
        ImageView moviePosterHeader = (ImageView) appBarLayout.findViewById(R.id.movie_poster_large);
        moviePosterFullscreen = (ImageView) activity.findViewById(R.id.movie_poster_fullscreen);

        moviePosterFullscreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moviePosterFullscreen.setVisibility(View.GONE);
            }
        });

        if (mMovie != null){
            appBarLayout.setTitle(mMovie.getTitle());
            Picasso.with(getContext()).load(Utils.getPosterLink(mMovie.getBackdrop_path(),
                    ServiceAPI.POSTER_SIZE_LARGE)).
                    placeholder(R.drawable.placeholder).
                    into(moviePosterHeader);

            Picasso.with(getContext()).load(Utils.getPosterLink(mMovie.getPoster_path(),
                    ServiceAPI.POSTER_SIZE_LARGE)).
                    placeholder(R.drawable.placeholder).
                    into(moviePosterFullscreen);
        }
    }

    private void setUpPoster(View rootView){
        moviePosterMini = (ImageView) rootView.findViewById(R.id.movie_poster_mini);
        Picasso.with(getContext()).load(Utils.getPosterLink(mMovie.getPoster_path(),
                ServiceAPI.POSTER_SIZE_LARGE)).
                placeholder(R.drawable.placeholder).
                into(moviePosterMini);

        moviePosterMini.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moviePosterFullscreen.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setUpLinks(View rootView){
        final LinearLayout imdbLayout = (LinearLayout) rootView.findViewById(R.id.imdb_line);
        final LinearLayout homepageLayout = (LinearLayout) rootView.findViewById(R.id.homepage_line);
        final TextView linksTitle = (TextView) rootView.findViewById(R.id.title_links);

        if (mMovie.getImdb_id() != null && !mMovie.getImdb_id().isEmpty()) {
            ((TextView) rootView.findViewById(R.id.imdb_link)).setText(Utils.
                    getIMDbLink(mMovie.getImdb_id()));
        } else {
            imdbLayout.setVisibility(View.GONE);
        }

        if (mMovie.getHomepage() != null && !mMovie.getHomepage().isEmpty()) {
            ((TextView) rootView.findViewById(R.id.homepage)).setText(mMovie.getHomepage());
        } else {
            homepageLayout.setVisibility(View.GONE);
        }

        if (imdbLayout.getVisibility() == View.GONE && homepageLayout.getVisibility() == View.GONE) {
            linksTitle.setVisibility(View.GONE);
        }
    }
}
