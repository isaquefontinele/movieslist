package com.example.isaque.movieslist.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.isaque.movieslist.R;
import com.example.isaque.movieslist.data.MoviesRetrievementService;
import com.example.isaque.movieslist.data.ServiceAPI;
import com.example.isaque.movieslist.model.MovieData;
import com.example.isaque.movieslist.model.MoviesListData;
import com.example.isaque.movieslist.utils.Constants;
import com.example.isaque.movieslist.utils.Utils;
import com.example.isaque.movieslist.view.MovieDetailFragment;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.example.isaque.movieslist.utils.Constants.ACTION_FLAG;
import static com.example.isaque.movieslist.utils.Constants.ACTION_GET_NOW_PLAYING;
import static com.example.isaque.movieslist.utils.Constants.ACTION_GET_POPULAR;
import static com.example.isaque.movieslist.utils.Constants.ACTION_GET_SEARCH;
import static com.example.isaque.movieslist.utils.Constants.ACTION_GET_UPCOMING;
import static com.example.isaque.movieslist.utils.Constants.ERROR;
import static com.example.isaque.movieslist.utils.Constants.ERROR_UNKNOWN;
import static com.example.isaque.movieslist.utils.Constants.MOVIE_ID;
import static com.example.isaque.movieslist.utils.Constants.PAGE;
import static com.example.isaque.movieslist.utils.Constants.RESULT_RECEIVER;
import static com.example.isaque.movieslist.utils.Constants.SEARCH_QUERY;

/**
 * Main activity.
 * Exibe um recicler view que exibe diferente listas de filmes.
 */
public class MovieListActivity extends BaseActivity {

    private boolean mTwoPane;
    private MoviesResultReceiverCallBack mReceiver;
    private List<MovieData> mMoviesList;
    private RecyclerView mRecyclerView;
    private SimpleItemRecyclerViewAdapter mAdapter;
    private TextView mMoviesNotFound;
    private TextView moviesListTitle;
    private Context mContext;
    private String mCurrentMovieListAction;
    private String mCurrentQuery;
    private int mCurrentPage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_list);
        mContext = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        if (findViewById(R.id.movie_detail_container) != null) {
            mTwoPane = true;
        }

        setupRecyclerView();
        setupSearchBar();
        setupReceiverForListMovies(ACTION_GET_POPULAR, mCurrentPage);
        setupFab();
    }

    private void setupSearchBar(){
        mCurrentQuery = "";
        SearchView searchView = (SearchView) findViewById(R.id.search_bar);
        searchView.setQueryHint(getString(R.string.search_hint));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                resetListParameters();
                mCurrentQuery = newText;
                if (!newText.isEmpty()) {
                    setupReceiverForSearchMoviesList(newText, mCurrentPage);
                    moviesListTitle.setVisibility(View.INVISIBLE);
                } else {
                    setupReceiverForListMovies(mCurrentMovieListAction, mCurrentPage);
                    moviesListTitle.setVisibility(View.VISIBLE);
                }
                return false;
            }
        });
        searchView.setIconifiedByDefault(false);
        searchView.setBackgroundColor(getColor(R.color.search_background));
        mRecyclerView.requestFocus();
    }

    private void setupReceiverForListMovies(String action, int page) {
        mCurrentMovieListAction = action;
        mReceiver = new MoviesResultReceiverCallBack(new Handler());
        Intent intent = new Intent(this, MoviesRetrievementService.class);
        intent.setAction(action);
        intent.putExtra(RESULT_RECEIVER, mReceiver);
        intent.putExtra(PAGE, page);
        startService(intent);
        showLoading();
    }

    private void setupReceiverForSearchMoviesList(String query, int page) {
        mReceiver = new MoviesResultReceiverCallBack(new Handler());
        Intent intent = new Intent(this, MoviesRetrievementService.class);
        intent.setAction(ACTION_GET_SEARCH);
        intent.putExtra(SEARCH_QUERY, query);
        intent.putExtra(PAGE, page);
        intent.putExtra(RESULT_RECEIVER, mReceiver);
        startService(intent);
    }

    private void setupRecyclerView() {
        mMoviesList = new ArrayList<>();
        resetListParameters();
        mRecyclerView = (RecyclerView) findViewById(R.id.movies_list);
        mMoviesNotFound = (TextView) findViewById(R.id.movie_list_not_found);
        moviesListTitle = (TextView) findViewById(R.id.movies_list_type);
        mAdapter = new SimpleItemRecyclerViewAdapter(mMoviesList);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(!isLoading()) {
                    if (!recyclerView.canScrollVertically(1)) {
                        mCurrentPage++;
                        if (!mCurrentQuery.isEmpty()) {
                            setupReceiverForSearchMoviesList(mCurrentQuery, mCurrentPage);
                        } else {
                            setupReceiverForListMovies(mCurrentMovieListAction, mCurrentPage);
                        }
                    }
                }
            }
        });
    }

    private void setupFab() {
        final FloatingActionButton fabPopular = (FloatingActionButton) findViewById(R.id.fab_action_popular);
        final FloatingActionButton fabUpcoming = (FloatingActionButton) findViewById(R.id.fab_action_upcoming);
        final FloatingActionButton fabPlayingNow = (FloatingActionButton) findViewById(R.id.fab_action_playing_now);
        final FloatingActionsMenu fabMenu = (FloatingActionsMenu) findViewById(R.id.fab_menu);

        fabPopular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabOnClick(ACTION_GET_POPULAR, fabMenu);
            }
        });
        fabPlayingNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabOnClick(ACTION_GET_NOW_PLAYING, fabMenu);
            }
        });
        fabUpcoming.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabOnClick(ACTION_GET_UPCOMING, fabMenu);
            }
        });
    }

    private void fabOnClick(String action, FloatingActionsMenu menu){
        if (!mCurrentMovieListAction.equals(action)) {
            resetListParameters();
            setupReceiverForListMovies(action, mCurrentPage);
        }
        menu.collapse();
    }

    private void resetListParameters(){
        mMoviesList.clear();
        mCurrentPage = 1;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    public class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.MovieHolder> {

        private List<MovieData> mValues;

        public SimpleItemRecyclerViewAdapter(List<MovieData> items) {
            mValues = items;
        }

        public List<MovieData> getMoviesList(){
            return mValues;
        }
        void setMoviesList(List<MovieData> movies){
            mValues = movies;
        }

        @Override
        public MovieHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.movie_list_content, parent, false);
            return new MovieHolder(view);
        }

        @Override
        public void onBindViewHolder(final MovieHolder movieHolder, final int position) {
            movieHolder.mItem = mValues.get(position);
            movieHolder.mTitle.setText(mValues.get(position).getTitle());
            movieHolder.mRating.setText(String.valueOf(mValues.get(position).getVote_average()));
            //Movie poster
            Picasso.with(mContext).
                    load(Utils.getPosterLink(mValues.get(position).getPoster_path(),
                            ServiceAPI.POSTER_SIZE_MINI)).
                    placeholder(R.drawable.placeholder).
                    into(movieHolder.mPoster);

            movieHolder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mTwoPane) {
                        Bundle arguments = new Bundle();
                        arguments.putInt(MOVIE_ID, mValues.get(position).getId());
                        MovieDetailFragment fragment = new MovieDetailFragment();
                        fragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.movie_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, MovieDetailActivity.class);
                        intent.putExtra(MOVIE_ID, mValues.get(position).getId());

                        context.startActivity(intent);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        class MovieHolder extends RecyclerView.ViewHolder {
            final View mView;
            final TextView mTitle;
            final TextView mRating;
            final ImageView mPoster;
            MovieData mItem;

            MovieHolder(View view) {
                super(view);
                mView = view;
                mTitle = (TextView) view.findViewById(R.id.movie_title);
                mRating = (TextView) view.findViewById(R.id.rating);
                mPoster = (ImageView) view.findViewById(R.id.movie_poster);
            }
        }
    }

    public class MoviesResultReceiverCallBack<T> extends ResultReceiver {

        public MoviesResultReceiverCallBack(Handler handler) {
            super(handler);
        }

        private void checkEmptyList(){
            if (mMoviesList.isEmpty()) {
                mMoviesNotFound.setVisibility(View.VISIBLE);
            } else {
                mMoviesNotFound.setVisibility(View.GONE);
            }
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            hideLoading();
            try {
                if (resultCode == Constants.RESULT_OK) {
                    if (mReceiver != null ){
                        switch (resultData.getString(ACTION_FLAG)){
                            case ACTION_GET_POPULAR:
                                mMoviesList.addAll(((MoviesListData)
                                        resultData.getSerializable(ACTION_GET_POPULAR)).
                                        getMoviesDataList());
                                moviesListTitle.setText(getString(R.string.movie_title_popular));
                                break;
                            case ACTION_GET_SEARCH:
                                mMoviesList.addAll(((MoviesListData)
                                        resultData.getSerializable(ACTION_GET_SEARCH)).
                                        getMoviesDataList());
                                break;
                            case ACTION_GET_UPCOMING:
                                mMoviesList.addAll(((MoviesListData)
                                        resultData.getSerializable(ACTION_GET_UPCOMING)).
                                        getMoviesDataList());
                                moviesListTitle.setText(getString(R.string.movie_title_upcoming));
                                break;
                            case ACTION_GET_NOW_PLAYING:
                                mMoviesList.addAll(((MoviesListData)
                                        resultData.getSerializable(ACTION_GET_NOW_PLAYING)).
                                        getMoviesDataList());
                                moviesListTitle.setText(getString(R.string.movie_title_now_playing));
                                break;
                        }
                        mAdapter.setMoviesList(mMoviesList);
                        mAdapter.notifyDataSetChanged();
//                        mRecyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(mMoviesList));

                        checkEmptyList();
                    }

                } else {
                    mMoviesList.clear();
                    mAdapter.notifyDataSetChanged();
                    checkEmptyList();
                    showError(resultData.getString(ERROR));
                }
            } catch (Exception e){
                showError(ERROR_UNKNOWN);
                e.printStackTrace();
            }
        }
    }

    /**
     * Get receiver para poder realizar os testes de serviço.
     * @return
     */
    public MoviesResultReceiverCallBack getReceiver() {
        return mReceiver;
    }
}
