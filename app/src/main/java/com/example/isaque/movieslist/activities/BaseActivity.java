package com.example.isaque.movieslist.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.isaque.movieslist.R;

import static com.example.isaque.movieslist.utils.Constants.ERROR;
import static com.example.isaque.movieslist.utils.Constants.ERROR_GETTING_DATA;
import static com.example.isaque.movieslist.utils.Constants.ERROR_UNKNOWN;
import static com.example.isaque.movieslist.utils.Constants.NETWORK_CONNECTION_ERROR;

public abstract class BaseActivity extends AppCompatActivity {

    ProgressDialog progress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progress = new ProgressDialog(this);
    }

    void showLoading(){
        progress.setTitle(getString(R.string.loading));
        progress.setMessage(getString(R.string.wait_while_loading));
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
    }

    void hideLoading(){
        progress.dismiss();
    }

    boolean isLoading() {
        return progress.isShowing();
    }

    void showError(String error){
        switch (error){
            case NETWORK_CONNECTION_ERROR:
                Toast.makeText(this, getString(R.string.network_error),
                        Toast.LENGTH_LONG).show();
                break;
            case ERROR_GETTING_DATA:
                Toast.makeText(this, getString(R.string.error_getting_data),
                        Toast.LENGTH_LONG).show();
                break;
            case ERROR_UNKNOWN:
                Toast.makeText(this, getString(R.string.unknown_error),
                        Toast.LENGTH_LONG).show();
                break;
        }
    }
}
