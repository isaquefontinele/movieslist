package com.example.isaque.movieslist.data;

import com.example.isaque.movieslist.model.MovieDetailsData;
import com.example.isaque.movieslist.model.MoviesListData;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

public interface ServiceAPI {

    String API_KEY = "30978da57eaa563e1135177016f56564";
    String BASE_URL = "http://api.themoviedb.org";

    String IMAGES_BASE_URL = "http://image.tmdb.org/t/p/";
    String POSTER_SIZE_MINI = "w185";
    String POSTER_SIZE_LARGE = "w780";

    String MOVIE_URL = "/3/movie/{id}";
    String POPULAR_MOVIES_URL = "/3/movie/popular";
    String MOVIE_SEARCH_URL = "/3/search/movie";
    String NOW_PLAYING_URL = "/3/movie/now_playing";
    String UPCOMING_URL = "/3/movie/upcoming";


    @GET(MOVIE_URL)
    Call<MovieDetailsData> getMovieByID(@Path("id") int id,
                                        @Query("api_key") String apiKey,
                                        @Query("language") String language);

    @GET(POPULAR_MOVIES_URL)
    Call<MoviesListData> getPopularMovies(@Query("api_key") String apiKey,
                                          @Query("page") int page,
                                          @Query("language") String language);

    @GET(MOVIE_SEARCH_URL)
    Call<MoviesListData> getMoviesListBySearch(@Query("api_key") String apiKey,
                                               @Query("query") String query,
                                               @Query("page") int page,
                                               @Query("language") String language);

    @GET(NOW_PLAYING_URL)
    Call<MoviesListData> getNowPlayingMovies(@Query("api_key") String apiKey,
                                             @Query("page") int page,
                                             @Query("language") String language);

    @GET(UPCOMING_URL)
    Call<MoviesListData> getUpcomingMovies(@Query("api_key") String apiKey,
                                           @Query("page") int page,
                                           @Query("language") String language);
}
