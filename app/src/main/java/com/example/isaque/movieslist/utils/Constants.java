package com.example.isaque.movieslist.utils;

public class Constants {

    public static final String ACTION_FLAG = "actionFlag";
    public static final String ACTION_GET_MOVIE = "getMovie";
    public static final String ACTION_GET_POPULAR = "getPopular";
    public static final String ACTION_GET_NOW_PLAYING = "getNowPlaying";
    public static final String ACTION_GET_UPCOMING = "getUpcoming";
    public static final String ACTION_GET_SEARCH = "getSearch";
    public static final String MOVIE_ID = "movieID";
    public static final String MOVIE_DATA = "movieData";
    public static final String SEARCH_QUERY = "searchQuery";

    public static final String ERROR = "error";
    public static final String NETWORK_CONNECTION_ERROR = "networkError";
    public static final String ERROR_GETTING_DATA = "getDataError";
    public static final String ERROR_UNKNOWN = "unknownError";

    public static final String RESULT_RECEIVER = "resultReceiver";
    public static final String PAGE = "page";
    public static final int RESULT_OK = 0;
    public static final int RESULT_FAIL = -1;

}
