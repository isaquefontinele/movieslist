package com.example.isaque.movieslist.model;


public class MovieData {

    private int id;
    private String title;
    private String poster_path;
    private String backdrop_path;
    private String release_date;
    private int[] genre_ids;
    private double vote_average;
    private double popularity;
    private String original_title;
    private String original_language;
    private String overview;


    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public String getRelease_date() {
        return release_date;
    }

    public int[] getGenre_ids() {
        return genre_ids;
    }

    public double getVote_average() {
        return vote_average;
    }

    public double getPopularity() {
        return popularity;
    }

    public String getOriginal_title() {
        return original_title;
    }

    public String getOriginal_language() {
        return original_language;
    }

    public String getOverview() {
        return overview;
    }
}
