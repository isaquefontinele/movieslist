package com.example.isaque.movieslist;

import com.example.isaque.movieslist.model.MovieDetailsData;
import com.example.isaque.movieslist.utils.Utils;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class MoviesListTests {

    /**
     * Testa o método getFormatedGenres.
     *
     * @throws Exception the exception
     */
    @Test
    public void getFormatedGenresTest() throws Exception {
        List<MovieDetailsData.GenreData> genres = new ArrayList<>();
        MovieDetailsData.GenreData genre = new MovieDetailsData.GenreData();


        //Normal input
        for (int i=0; i<5; i++){
            genre = new MovieDetailsData.GenreData();
            genre.setName("Genre " + i);
            genres.add(genre);
        }
        String expected = "Genre 0, Genre 1, Genre 2, Genre 3, Genre 4";
        String actual = Utils.getFormattedGenres(genres);
        assertEquals("Genres formating, failed.", expected, actual);


        //1 item input
        genres.clear();
        genre = new MovieDetailsData.GenreData();
        genre.setName("Single entry");
        genres.add(genre);
        expected = "Single entry";
        actual = Utils.getFormattedGenres(genres);
        assertEquals("Genres formating, failed.", expected, actual);


        //Empty input
        genres.clear();
        expected = "";
        actual = Utils.getFormattedGenres(genres);
        assertEquals("Genres formating, failed.", expected, actual);


        //Null input
        expected = "";
        actual = Utils.getFormattedGenres(null);
        assertEquals("Genres formating, failed.", expected, actual);

    }
}
