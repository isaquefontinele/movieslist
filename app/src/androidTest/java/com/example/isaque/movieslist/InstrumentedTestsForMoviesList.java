package com.example.isaque.movieslist;

import android.content.Context;
import android.content.Intent;
import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.ViewAssertion;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.example.isaque.movieslist.activities.MovieListActivity;
import com.example.isaque.movieslist.data.MoviesRetrievementService;
import com.example.isaque.movieslist.model.MovieData;

import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.ThreadLocalRandom;

import static android.support.test.InstrumentationRegistry.getTargetContext;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeUp;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withHint;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.example.isaque.movieslist.utils.Constants.ACTION_GET_NOW_PLAYING;
import static com.example.isaque.movieslist.utils.Constants.ACTION_GET_POPULAR;
import static com.example.isaque.movieslist.utils.Constants.ACTION_GET_UPCOMING;
import static com.example.isaque.movieslist.utils.Constants.PAGE;
import static com.example.isaque.movieslist.utils.Constants.RESULT_RECEIVER;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class InstrumentedTestsForMoviesList {

    @Rule
    public ActivityTestRule<MovieListActivity> mActivitRule =
            new ActivityTestRule<>(MovieListActivity.class);

    @Before
    public void waitService() throws InterruptedException {
        Thread.sleep(1500);
    }
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = getTargetContext();

        assertEquals("com.example.isaque.movieslist", appContext.getPackageName());
    }

    /**
     * Checks the title above the list of movies.
     * @throws InterruptedException
     */
    @Test
    public void checkMoviesListTitle() throws InterruptedException {
        //Checks the default title is Popular Movies
        onView(withId(R.id.movies_list_type)).check(matches(withText(R.string.movie_title_popular)));

        //Click on FAB and check it changed to Now Playing
        onView(withId(R.id.fab_action_playing_now)).perform(click());
        onView(withId(R.id.fab_action_playing_now)).perform(click());
        waitService();
        onView(withId(R.id.movies_list_type)).check(matches(withText(R.string.movie_title_now_playing)));

        //Click on FAB and check it changed to Upcoming
        onView(withId(R.id.fab_action_upcoming)).perform(click());
        onView(withId(R.id.fab_action_upcoming)).perform(click());
        waitService();
        onView(withId(R.id.movies_list_type)).check(matches(withText(R.string.movie_title_upcoming)));

        //Click on FAB and check it changed back to Popular
        onView(withId(R.id.fab_action_popular)).perform(click());
        onView(withId(R.id.fab_action_popular)).perform(click());
        waitService();
        onView(withId(R.id.movies_list_type)).check(matches(withText(R.string.movie_title_popular)));
    }

    /**
     * Normal search bar result test.
     * @throws InterruptedException
     */
    @Test
    public void searchBarTest() throws InterruptedException {
        //Types "lion king" on the search bar, slowly, so the onTextChange can trigger correctly
        onView(withHint(R.string.search_hint)).perform(typeText("lion kin"));
        waitService();
        onView(withHint(R.string.search_hint)).perform(typeText("g"));
        waitService();

        //Checks if the first item of the search, has the title "The Lion King"
        onView(nthChildOf(withId(R.id.movies_list), 0))
                        .check(matches(hasDescendant(withText("The Lion King"))));
    }

    /**
     * Types random sequence letters on the search bar, to get an empty result.
     * @throws InterruptedException
     */
    @Test
    public void searchBarEmptyResultTest() throws InterruptedException {
        onView(withHint(R.string.search_hint)).perform(typeText("ilashgisuehfsa"));
        waitService();
        onView(withHint(R.string.search_hint)).perform(typeText("z"));
        waitService();

        checkEmptyRecyclerView();
    }

    /**
     * Checks if the recycler view has 0 items and if the message "No movies found" shows up.
     */
    private void checkEmptyRecyclerView(){
        onView(withId(R.id.movies_list)).check(new RecyclerViewItemCountAssertion(0));
        onView(withId(R.id.movie_list_not_found)).check(matches(isDisplayed()));
    }

    /**
     * Checks if: the recycler view is visible, have 20 items, and the error message doesn't show up.
     */
    private void checkNormalRecyclerView(int numItems){
        onView(withId(R.id.movies_list)).check(matches(isDisplayed()));
        onView(withId(R.id.movies_list)).check(new RecyclerViewItemCountAssertion(numItems));
        onView(withId(R.id.movie_list_not_found)).check(matches(not(isDisplayed())));
    }

    /**
     * Scrolls the recycler to the bottom, so it load more items.
     * @throws InterruptedException
     */
    @Test
    public void recyclerViewScrollToBottomLoadMoreItems() throws InterruptedException {
        //Check if the recycler loaded 20 items by default
        onView(withId(R.id.movies_list)).check(new RecyclerViewItemCountAssertion(20));

        //Scroll to the bottom
        onView(withId(R.id.movies_list)).perform(RecyclerViewActions.scrollToPosition(19));
        onView(withId(R.id.movies_list)).perform(swipeUp());
        waitService();

        //Check it loaded 20 more items
        onView(withId(R.id.movies_list)).check(new RecyclerViewItemCountAssertion(40));
    }

    /**
     * Makes a normal correct to PopularMovies.
     * @throws Throwable
     */
    @Test
    public void normalRequisitionPopular() throws Throwable {
        makeCorrectRequisitionMoviesList(ACTION_GET_POPULAR);
    }

    /**
     * Makes a normal correct to UpcomingMovies.
     * @throws Throwable
     */
    @Test
    public void normalRequisitionUpcoming() throws Throwable {
        makeCorrectRequisitionMoviesList(ACTION_GET_UPCOMING);
    }

    /**
     * Makes a normal correct to PlayingNowMovies.
     * @throws Throwable
     */
    @Test
    public void normalRequisitionPlayingNow() throws Throwable {
        makeCorrectRequisitionMoviesList(ACTION_GET_NOW_PLAYING);
    }

    private void makeCorrectRequisitionMoviesList(String action) throws InterruptedException {
        int page = 1;

        MovieListActivity.MoviesResultReceiverCallBack retrievementService = mActivitRule.getActivity().getReceiver();
        Intent intent = new Intent(mActivitRule.getActivity(), MoviesRetrievementService.class);
        intent.setAction(action);
        intent.putExtra(RESULT_RECEIVER, retrievementService);
        intent.putExtra(PAGE, page);

        mActivitRule.getActivity().startService(intent);
        waitService();

        // Since there was already 20 items loaded by default,
        // when we call the service again, there will be 40.
        checkNormalRecyclerView(40);
    }

    /**
     * It must return a normal result, because page is an optional parameter.
     * @throws Throwable
     */
    @Test
    public void requisitionMissingPageParameter() throws Throwable {

        String action = ACTION_GET_POPULAR;

        MovieListActivity.MoviesResultReceiverCallBack retrievementService = mActivitRule.getActivity().getReceiver();
        Intent intent = new Intent(mActivitRule.getActivity(), MoviesRetrievementService.class);
        intent.setAction(action);
        intent.putExtra(RESULT_RECEIVER, retrievementService);

        mActivitRule.getActivity().startService(intent);
        waitService();

        checkNormalRecyclerView(40);
    }

    /**
     * Page must be >=1.
     * @throws Throwable
     */
    @Test
    public void requisitionWithWrongPage() throws Throwable {

        String action = ACTION_GET_POPULAR;
        int page = 0;

        MovieListActivity.MoviesResultReceiverCallBack retrievementService = mActivitRule.getActivity().getReceiver();
        Intent intent = new Intent(mActivitRule.getActivity(), MoviesRetrievementService.class);
        intent.setAction(action);
        intent.putExtra(RESULT_RECEIVER, retrievementService);
        intent.putExtra(PAGE, page);

        mActivitRule.getActivity().startService(intent);
        waitService();

        checkEmptyRecyclerView();
    }

    /**
     * You HAVE to provide an action, so the service knows which method to call.
     * @throws Throwable
     */
    @Test
    public void requisitionMissingAction() throws Throwable {

        int page = 1;

        MovieListActivity.MoviesResultReceiverCallBack retrievementService = mActivitRule.getActivity().getReceiver();
        Intent intent = new Intent(mActivitRule.getActivity(), MoviesRetrievementService.class);
        intent.putExtra(RESULT_RECEIVER, retrievementService);
        intent.putExtra(PAGE, page);

        mActivitRule.getActivity().startService(intent);
        waitService();

        checkEmptyRecyclerView();
    }


    /**
     * Checks if the MovieDetailsActivity exhibits the correct information from the selected movie.
     * @throws Throwable
     */
    @Test
    public void clickOnMovie() throws Throwable {
        //Saves the data from the movie on position 0
        int position = 0;
        RecyclerView recyclerView =
                (RecyclerView) mActivitRule.getActivity().findViewById(R.id.movies_list);
        MovieListActivity.SimpleItemRecyclerViewAdapter adapter =
                (MovieListActivity.SimpleItemRecyclerViewAdapter) recyclerView.getAdapter();
        MovieData movieData = adapter.getMoviesList().get(position);

        // Clicks on the movie
        onView(nthChildOf(withId(R.id.movies_list), position)).perform(click());
        waitService();

        // Check if the info being displayed are from the selected movie.
        onView(withId(R.id.movieID)).check(matches(withText(String.valueOf(movieData.getId()))));
        onView(withId(R.id.name)).check(matches(withText(movieData.getOriginal_title())));
        onView(withId(R.id.release_date)).check(matches(withText(movieData.getRelease_date())));
        onView(withId(R.id.rating)).check(matches(withText(String.valueOf(movieData.getVote_average()))));
    }

    /**
     * Clicks on a random movie, and then press onBack to check if it returns to the first activity.
     * Performs 3 times.
     * @throws Throwable
     */
    @Test
    public void onBackPressed() throws Throwable {
        int MIN = 0;
        int MAX = 6;
        int randomNum = ThreadLocalRandom.current().nextInt(MIN, MAX);
        executesOnBack(randomNum);
        randomNum = ThreadLocalRandom.current().nextInt(MIN, MAX);
        executesOnBack(randomNum);
        randomNum = ThreadLocalRandom.current().nextInt(MIN, MAX);
        executesOnBack(randomNum);
    }

    private void executesOnBack(int position) throws InterruptedException {
        // Clicks on the movie at received position
        onView(nthChildOf(withId(R.id.movies_list), position)).perform(click());
        waitService();

        // Press on back
        onView(isRoot()).perform(ViewActions.pressBack());
        waitService();

        // Check if is back to previous activity
        checkNormalRecyclerView(20);
    }



    //-- Métodos auxiliares para os testes. --//
    private static Matcher<View> nthChildOf(final Matcher<View> parentMatcher, final int childPosition) {
        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(org.hamcrest.Description description) {
                description.appendText("with "+childPosition+" child view of type parentMatcher");
            }

            @Override
            public boolean matchesSafely(View view) {
                if (!(view.getParent() instanceof ViewGroup)) {
                    return parentMatcher.matches(view.getParent());
                }

                ViewGroup group = (ViewGroup) view.getParent();
                return parentMatcher.matches(view.getParent()) && group.getChildAt(childPosition).equals(view);
            }
        };
    }

    public class RecyclerViewItemCountAssertion implements ViewAssertion {
        private final int expectedCount;

        public RecyclerViewItemCountAssertion(int expectedCount) {
            this.expectedCount = expectedCount;
        }

        @Override
        public void check(View view, NoMatchingViewException noViewFoundException) {
            if (noViewFoundException != null) {
                throw noViewFoundException;
            }

            RecyclerView recyclerView = (RecyclerView) view;
            RecyclerView.Adapter adapter = recyclerView.getAdapter();
            assertThat(adapter.getItemCount(), is(expectedCount));
        }
    }

}
